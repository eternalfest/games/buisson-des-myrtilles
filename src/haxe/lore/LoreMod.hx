package lore;

import patchman.IPatch;
import merlin.IAction;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;
import hf.mode.GameMode;
import lore.actions.Lore;
import etwin.ds.WeakMap;
import hf.Lang;
import merlin.Merlin;
import merlin.value.MerlinFloat;

import keyboard.Key;
import keyboard.KeyCode;

@:build(patchman.Build.di())
class LoreMod {

	@:diExport
    public var lore(default, null): IAction;

	public static var SPRITES: WeakMap<GameMode, PauseLore> = new WeakMap();

	public var hml : HostModuleLoader;
    
    public function new(hml: HostModuleLoader){
        this.hml = hml;
        this.lore = new Lore(this);
    };
	
	@:diExport
	public var gestionPause(default, null): IPatch = Ref
		.auto(GameMode.onUnpause)
		.wrap(function(hf, self, old){
			if(LoreMod.getPauseLore(self) == null)
				old(self);
		});
	
	@:diExport
	public var initLoreConstante(default, null): IPatch = Ref
		.auto(GameMode.initWorld)
		.before(function(hf, self) : Void{
			Merlin.setGlobalVar(self, Obfu.raw("LAST_LORE"), (-1.0: MerlinFloat));
		});
	
	@:diExport
	public var validerLore(default, null): IPatch = Ref
		.auto(GameMode.getControls)
		.before(function(hf, self) : Void{
			if(LoreMod.getPauseLore(self) != null){
				if (Key.isDown(KeyCode.SPACE) && self.keyLock != KeyCode.SPACE.toInt()) {
					LoreMod.supprimerLore(self);
					self.keyLock = KeyCode.SPACE.toInt();
				}
				else if (Key.isDown(KeyCode.UP) && self.keyLock != KeyCode.UP.toInt()) {
					LoreMod.getPauseLore(self).up();
					LoreMod.getPauseLore(self).update(self);
					self.keyLock = KeyCode.UP.toInt();
				}
				else if (Key.isDown(KeyCode.DOWN) && self.keyLock != KeyCode.DOWN.toInt()) {
					LoreMod.getPauseLore(self).down();
					LoreMod.getPauseLore(self).update(self);
					self.keyLock = KeyCode.DOWN.toInt();
					
				}
			}
		});
	
	public function afficherLore(game: GameMode, id: Int, liste : Null<Array<Int>>): Void {
		if(LoreMod.getPauseLore(game) != null)
			return;
		game.lock();
		game.world.lock();
		if (!game.fl_mute) {
			game.setMusicVolume(0.5);
		}
		game.fl_pause = true;
		var msg = game.root.Lang.get(id);
			msg = LoreMod.calculeHTML(game, msg, this.hml);
			msg = "<p align=\"justify\"><font color=\"#D8BEA9\">"+msg+"</font></p>";
		var ecran = PauseLore.attach(game);
			ecran._x -= 10;
			ecran.setText(msg);
			if (liste == null){
				ecran.listeReponses = null;
			}else{
				ecran.listeReponses = new Array();
				for (i in liste){
					ecran.listeReponses.push(StringTools.replace(game.root.Lang.get(i), "{playerName}", LoreMod.getUserDisplayNameFromHml(hml)));
				}
			}
			ecran.update(game);
		SPRITES.set(game, ecran);
	}
	
	public static function supprimerLore(game: GameMode): Void {
		var sprite = LoreMod.getPauseLore(game);
		if(sprite != null){
			PauseLore.LAST_LORE = sprite.currentReponse;
			patchman.DebugConsole.log("Lore : choix n°" + Std.string(PauseLore.LAST_LORE));
			Merlin.setGlobalVar(game, Obfu.raw("LAST_LORE"), (sprite.currentReponse: MerlinFloat));
			sprite.removeMovieClip();
			LoreMod.SPRITES.remove(game);
			game.onUnpause();
		}
	}
	
	public static function calculeHTML(g : GameMode,texte : String, hml : HostModuleLoader): String{
		texte = g.root.Data.replaceTag(texte, "*", "<font color=\"#f7e8d5\">", "</font>");	
		texte = g.root.Data.replaceTag(texte, "$", "<b><font color=\"#423224\">", "</font></b>");
		texte = g.root.Data.replaceTag(texte, "|", "<i>", "</i>");
		texte = StringTools.replace(texte, "{playerName}", LoreMod.getUserDisplayNameFromHml(hml));
		return texte;
	}
	
	public static function getUserDisplayNameFromHml(hml: HostModuleLoader): Dynamic {
        var runMod: Dynamic = hml.require(Obfu.raw("run"));
        var runData: Dynamic = Reflect.callMethod(runMod, Reflect.field(runMod, Obfu.raw("getRun")), []);
        var user: Dynamic = Reflect.field(runData, Obfu.raw("user"));
        var userDisplayName: Dynamic = Reflect.field(user, Obfu.raw("displayName"));
        return userDisplayName;
    }
	
	public static function getPauseLore(game: GameMode): Null<PauseLore> {
		return LoreMod.SPRITES.get(game);
	}
}