package lore.actions;

import hf.Lang;
import hf.mode.GameMode;
import lore.PauseLore;
import merlin.IAction;
import merlin.IActionContext;
import patchman.HostModuleLoader;
import etwin.Obfu;

import hf.mode.GameMode;
import lore.actions.Lore;

class Lore implements IAction {

	public var name(default, null): String = Obfu.raw("lore");
	public var isVerbose(default, null): Bool = true;
	public var mod : LoreMod;

	public var hml : HostModuleLoader;
    
    public function new(mod : LoreMod) {
        this.mod = mod;
    }

	 public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();
        var id: Int = ctx.getInt(Obfu.raw("id"));
		
		var liste : Null<Array<Int>> = new Array();
		var k = 0;
		while (true) {
			var rep = ctx.getOptInt(Obfu.raw("rep")+Std.string(k));
			liste.push(rep.or(_ => break));
			k = k + 1;
		}
		if (liste.length == 0)
			liste = null;
        this.mod.afficherLore(game, id, liste);
        return false;
    }
}