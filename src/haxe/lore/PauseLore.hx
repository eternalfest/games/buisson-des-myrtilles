package lore;

import StringTools;
import hf.Hf;
import hf.mode.GameMode;
import hf.Data;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;

class PauseLore extends etwin.flash.MovieClip{
  
	public var msg 			: etwin.flash.TextField;
	public var hml 			: HostModuleLoader;
	public var confirmation	: etwin.flash.TextField;
	public var msgTexte		: String = "";
	
	public var listeReponses : Null<Array<String>> = null;
	public var currentReponse : Int = 0;
	
	public static var TEXTE_POS_X 		: Float =  85;
	public static var TEXTE_POS_Y 		: Float = 150;
	public static var TEXTE_LONGUEUR 	: Float = 250;
	public static var LAST_LORE			: Null<Int> = null;
	
	public function new(hml : HostModuleLoader) {
		this.hml = hml;
	};
	
	public static function attach(g: GameMode): PauseLore {
		var hf : Hf = g.root;
		hf.Std.registerClass(Obfu.raw("$pauseLore"), PauseLore);
		var parchemin : PauseLore = cast g.depthMan.attach(Obfu.raw("$pauseLore"), g.root.Data.DP_INTERF);
		
			parchemin.msg 			= g.root.Std.createTextField(parchemin, parchemin.getNextHighestDepth());
			parchemin.msg._x		= PauseLore.TEXTE_POS_X;
			parchemin.msg._y 		= PauseLore.TEXTE_POS_Y;
			parchemin.msg._width 	= PauseLore.TEXTE_LONGUEUR;
			parchemin.msg._height 	= PauseLore.TEXTE_LONGUEUR;
			parchemin.msg.multiline = true;
			parchemin.msg.wordWrap 	= true;
			parchemin.msg.html 		= true;
			
			parchemin.confirmation 				= g.root.Std.createTextField(parchemin, parchemin.getNextHighestDepth());
			parchemin.confirmation._width 		= PauseLore.TEXTE_LONGUEUR;
			parchemin.confirmation._height 		= 60;
			parchemin.confirmation._x 			= PauseLore.TEXTE_POS_X;
			parchemin.confirmation._y 			= 330;
			parchemin.confirmation.html 		= true;
			parchemin.confirmation.multiline	= true;
			parchemin.confirmation.wordWrap 	= true;
			parchemin.confirmation.htmlText		= "<p align=\"center\"><font color=\"#FFFFFF\">" + hf.Lang.get(75) + "</font></p>";
		return parchemin;
	}
	
	public function setText(texte : String):Void{
		this.msgTexte = texte;
		this.msg.htmlText = texte;
	}
	
	public function down():Void{
		if (this.listeReponses != null){
			this.currentReponse = (this.currentReponse + 1)%this.listeReponses.length;
		}
	}
	public function up():Void{
		if (this.listeReponses != null){
			this.currentReponse = (this.currentReponse + this.listeReponses.length - 1)%this.listeReponses.length;
		}
	}
	
	public function update(game : GameMode):Void{
		this.msg.htmlText = this.msgTexte+"<p></br></p>";
		if (this.listeReponses != null){
			for (i in 0...this.listeReponses.length){
				var rep = this.listeReponses[i];
				if(i == this.currentReponse)
					this.msg.htmlText += "<p align=\"center\"><font  color=\"#FFFFFF\">&gt;" +rep+ "</font></p>";
				else
					this.msg.htmlText += "<p align=\"center\"><font  color=\"#404040\">"+rep+ "</font></p>";
			}
		}
		if (this.listeReponses == null || this.listeReponses.length == 1){
			this.confirmation._y = 350;
			this.confirmation.htmlText = "<p align=\"center\"><font color=\"#FFFFFF\">" + game.root.Lang.get(75) + "</font></p>";
		}
		else{
			this.confirmation._y = 330;
			this.confirmation.htmlText = "<p align=\"center\"><font color=\"#FFFFFF\">" + game.root.Lang.get(76) + "</font></p>";
		}
		
	}
}