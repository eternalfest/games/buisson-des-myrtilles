package openSortie.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class OpenSortie implements IAction {

  public var name(default, null): String = Obfu.raw("openSortie");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var x: Null<Float> = ctx.getOptFloat(Obfu.raw("x"));
		var y: Null<Float> = ctx.getOptFloat(Obfu.raw("y"));
		var pid: Null<Float> = ctx.getOptFloat(Obfu.raw("pid"));
		if (game.portalMcList[pid] != null) {
			return false;
		}
		game.world.scriptEngine.insertPortal(x, y, pid);
		var v5 = game.root.Entity.x_ctr(game.flipCoordCase(x));
		var v6 = game.root.Entity.y_ctr(y) - game.root.Data.CASE_HEIGHT * 0.5;
		var v7 = game.depthMan.attach("$sortie", game.root.Data.DP_SPRITE_BACK_LAYER);
			v7._x = v5;
			v7._y = v6;
		game.fxMan.attachExplosion(v5, v6, 30);
		game.fxMan.inGameParticles(game.root.Data.PARTICLE_PORTAL, v5, v6, 5);
		game.fxMan.attachShine(v5, v6);
		game.portalMcList[pid] = {x: v5, y: v6, mc: v7, cpt: 0};
		return true;
	}
}