package laby;

import patchman.Ref;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import hf.Hf;
import etwin.Obfu;

import hf.entity.Player;
import hf.Data;
import hf.mode.GameMode;

import merlin.Merlin;
import merlin.value.MerlinValue;

@:build(patchman.Build.di())
class Laby {

	public static var LONG : Int = 4;
	public static var HAUTEUR : Int = 2;
	public static var PERDU : String = Obfu.raw("perdu");
	public static var VILLAGE : String = Obfu.raw("village");
	
	public var position : Int = 0;
	public var laby : Array<Int>;

	@:diExport
	public var checkPerdu(default, null): IPatch;
	
	public static function estDansListe<T>(liste : Array<T>, element : T) : Bool{
		for (i in liste){
			if( element == i){
				return true;
			}
		}
		return false;
	}
	
	public  function new(){
		this.laby = new Array();
		this.position = 1;
		
		this.checkPerdu = Ref.auto(GameMode.usePortal).wrap(function(hf, self, pid : Int, e, old) : Bool{
			var isLaby = Merlin.getGlobalVar(self, Obfu.raw("LABY"))
				.map(v => v.unwrapBool())
				.or(false);
			if(isLaby) {
				this.laby.push(this.position);
				var npos = this.position;
				if(pid == 0){
					if( npos > HAUTEUR){
						npos -= HAUTEUR + 1;
					}else{
						npos += LONG * HAUTEUR + 1;
					}
				}else if(pid == 1){
					if( npos > LONG * HAUTEUR){
						npos -= LONG * HAUTEUR + 1;
					}else{
						npos += HAUTEUR + 1;
					}
				}
				if(estDansListe(this.laby,npos)){
					var reference = hf.Data.getLevelFromTag(PERDU);
					self.switchDimensionById(reference.did, reference.lid + this.position%(HAUTEUR + 1)-1, -1);
					return true;
				}
				else{
					this.position = npos;
					return old(self, pid,e);
				}
			}else{
				if(pid == 10){
					this.position ++;
					this.laby = new Array();
					patchman.DebugConsole.log(VILLAGE + "+" + Std.string(this.position));
					var reference = hf.Data.getLevelFromTag(VILLAGE + "+" + Std.string(this.position));
					patchman.DebugConsole.log(reference.lid);
					self.switchDimensionById(reference.did, reference.lid, -1);
					return true;
				}
				return old(self, pid, e);
			}
		});
	}
}