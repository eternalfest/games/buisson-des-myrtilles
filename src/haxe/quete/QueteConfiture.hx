package quete;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.entity.Player;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class QueteConfiture {

    public function new() {}
    
    public static function queteConfiture(quete : Quetes) : IPatch{
        return Ref.auto(hf.entity.Player.initPlayer)
        .before(function(hf, self,g: hf.mode.GameMode, x: Float, y: Float){
            if (quete.estQueteCompletee(0)){
                ++self.lives;
            }
        });
    }
}