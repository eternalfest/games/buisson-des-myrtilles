import patchman.Patchman;
import patchman.IPatch;
import hf.Hf;

import merlin.Merlin;
import merlin.IAction;

import buisson.Buisson;
import buisson.DecorsBranche;
import buisson.BossLevel;
import quete.Quetes;
import debug.Debug;
import lore.LoreMod;
import laby.Laby;
import utilitaire.PlayerName;
import utilitaire.FixedBackground;

import patchman.HostModuleLoader;
import etwin.Obfu;


@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }
  

  public function new(
	debug: Debug,
	decors01 : DecorsBranche,
	buisson: Buisson,
	quete : Quetes,
	loreMod : LoreMod,
	merlin: Merlin,
	laby : Laby,
	playerName : PlayerName,
	bossLevel : BossLevel,
	patches: Array<IPatch>, hf: Hf) {
	  Patchman.patchAll(patches, hf);
    }
}
