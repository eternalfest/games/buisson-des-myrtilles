package buisson;

import hf.entity.bad.walker.Pomme;
import hf.mode.GameMode;
import hf.Hf;
import etwin.Obfu;
import hf.Animation;
import hf.Data;
import hf.Timer;
import buisson.BulleInvocation;

import color_matrix.ColorMatrix;

@:hfTemplate
class BossMyrtille extends Pomme {
	
	public static var ID 					: Int = 102;
	public static var MAX_BULLES 			: Int = 5;
	public static var MAX_VIES 				: Int = 51;
	public static var PALIER_VIES_RAGE_1	: Float = 30;
	public static var PALIER_VIES_RAGE_2	: Float = 45;
	public static var TEMPS_INVULNERABILITE : Float = 3.5;
	public static var TEMPS_DIFF_BULLES		: Float = 3;
	
	public var estInvulnerable				: Bool = true;
	public var tempsInvulnerable			: Float;
	public var tempsDiffBullesRestant		: Float;
	public var vie							: Int = BossMyrtille.MAX_VIES;
	public var thermometre					: Dynamic;
	public var bulle						: Dynamic;
	public var intro						: Bool = true;
	
	public function new(): Void {
		super(null);
		this.filters = [ColorMatrix.fromHsv(150,1, 0.8).toFilter()];
		this.animFactor = 0.65;
		this.setJumpUp(0);
		this.setJumpH(0);
		this.setClimb(0, 3);
		this.setShoot(0);
	}
	
	public static function attach(g: GameMode, x: Float, y: Float): BossMyrtille {
		var hf : Hf = g.root;
		hf.Std.registerClass(Obfu.raw("boss_myrtille"), BossMyrtille.getClass(hf));
		var bad : BossMyrtille = cast g.depthMan.attach(Obfu.raw("boss_myrtille"), hf.Data.DP_BADS);
			bad.initBad(g, x, y);
			bad.scale(180);
			bad.thermometre = g.depthMan.attach("hammer_interf_boss_bar", hf.Data.DP_INTERF);
			bad.thermometre._rotation = -90;
			bad.thermometre._x = 0;
			bad.thermometre._y = hf.Data.GAME_HEIGHT * 0.5;
			bad.register(hf.Data.BOSS);
			bad.fl_moveable = false;
			bad.fl_alphaBlink = false;
			bad.blinkColor = 16752222;
			bad.blinkColorAlpha = 50;
			bad.bulle = g.world.view.attachSprite("$bulle", x+40 , y-30, false);
		return bad;
	}
	
	override public function dropReward(): Void {}
	
	override public function onShoot(): Void {
		this.tempsDiffBullesRestant = BossMyrtille.TEMPS_DIFF_BULLES * this.game.root.Data.SECOND;
		var enervement : Int = 0;
		if (this.vie < BossMyrtille.PALIER_VIES_RAGE_1)
			enervement++;
		if (this.vie < BossMyrtille.PALIER_VIES_RAGE_2)
			enervement++;
		var vitesse : Float =  1.5 + (BossMyrtille.MAX_VIES - this.vie) / BossMyrtille.MAX_VIES * 1.75;
		var bulle : BulleInvocation = BulleInvocation.attach(this.game, this.x, this.y, enervement,vitesse);
			bulle.y -= 20;
		if (this.dir < 0) {
			bulle.moveLeft(vitesse);
			bulle.x -= 40;
		} else {
			bulle.moveRight(vitesse);
			bulle.x += 40;
		}
		patchman.DebugConsole.log(this.vie);
	}
	
	public function changeIntro() : Void{
		this.intro = false;
		this.estInvulnerable = false;
		this.bulle._visible = false;
		this.setJumpUp(5);
		this.setJumpH(100);
		this.setClimb(100, 3);
		this.setShoot(3);
	}
	
	override public function update(): Void {
		this.game.huTimer = 0;
		if (this.tempsDiffBullesRestant > 0) {
			this.tempsDiffBullesRestant -= this.game.root.Timer.tmod;
		}
		if (this.tempsInvulnerable > 0) {
			this.tempsInvulnerable -= this.game.root.Timer.tmod;
			if (this.tempsInvulnerable <= 0){
				this.estInvulnerable = false;
				this.stopBlink();
			}
		}
		this.bulle._x = this.x;
		this.bulle._y = this.y;
		this.fl_shooter = (this.tempsDiffBullesRestant <= 0 && this.game.getBadList().length <= BossMyrtille.MAX_BULLES);
		super.update();
	}
	
	public function updateBar(): Void {
		this.thermometre.barFade._xscale = this.thermometre.bar._xscale;
		this.thermometre.barFade.gotoAndPlay("1");
		this.thermometre.bar._xscale = (this.vie / BossMyrtille.MAX_VIES) * 100;
	}
	
	public function invulnerabilise(): Void {
		this.estInvulnerable = true;
		this.tempsInvulnerable = BossMyrtille.TEMPS_INVULNERABILITE * this.game.root.Data.SECOND;
		this.blink(null/*BossMyrtille.TEMPS_INVULNERABILITE * this.game.root.Data.SECOND*/);
	}
	
	public function perdVies(n : Int) : Void{
		if(this.estInvulnerable)
			return;
		this.vie -= n;
		this.updateBar();
		this.tempsDiffBullesRestant = BossMyrtille.TEMPS_DIFF_BULLES * this.game.root.Data.SECOND;
		if(this.vie > 0){
			this.invulnerabilise();
			if(n > 1){
				this.game.shake(this.game.root.Data.SECOND, 4);
				this.game.destroyList(this.game.root.Data.BAD_BOMB);
				var listeJoueurs = this.game.getPlayerList();
				for (joueur in listeJoueurs) {
					joueur.knock(this.game.root.Data.SECOND);
				}
				var listeFruits = this.game.getBadList();
				for (fruit in listeFruits) {
					if (this.fl_stable) {
						fruit.dx = 0;
					}
					if(Std.is(fruit, Myrtille)){
						var myrtille: Myrtille = cast fruit;
							myrtille.unshield();
					}
					fruit.knock(this.game.root.Data.KNOCK_DURATION);
				}
			}
		}else{
			this.destroy();
		}
	}
	
	override public function freeze(timer: Float) : Void{
		if (this.fl_kill || this.estInvulnerable) {
			return;
		}
		this.perdVies(1);
	}
	
	override public function destroy() : Void{
		this.thermometre.removeMovieClip();
		var listeFruits = this.game.getBadList();
		for(fruit in listeFruits){
			if(fruit.uniqId != this.uniqId){
				fruit.destroy();
			}
		}
		this.game.destroyList(this.game.root.Data.BAD_BOMB);
		var explosion = this.game.world.view.attachSprite("$explosionPad", this.x - 200 , this.y - 180, true);
		this.game.huTimer = this.game.root.Data.HU_STEPS[this.game.huState] / this.game.diffFactor - this.game.root.Data.SECOND;
		super.destroy();
	}
	
	override public function killHit(dx: Null<Float>): Void{
		if (this.fl_kill || this.estInvulnerable) {
			return;
		}
		this.perdVies(10);
	}
	
	override public function hit(e): Void{
		super.hit(e);
		if ((e.types & this.game.root.Data.PLAYER_BOMB) > 0) {
			this.game.fxMan.attachExplosion(e.x, e.y, 20);
			e.destroy();
		}
		if ((e.types & this.game.root.Data.BAD) > 0) {
			this.game.fxMan.attachExplosion(e.x, e.y, 20);
			e.destroy();
		}
	}
}