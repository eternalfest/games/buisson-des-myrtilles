package buisson;

import patchman.IPatch;
import merlin.IAction;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;
import utilitaire.FixedBackground;

@:build(patchman.Build.di())
class Buisson {
  public static var PAIX_INTERIEURE 	: Int = 6;
  public static var SURPRISE_DE_PAILLE 	: Int = 86;
  public var hml : HostModuleLoader;

  @:diExport
  public var fixed_background(default, null): IAction;
  
  @:diExport
  public var ninjutsu(default, null): IPatch;
  
  @:diExport
  public var registerMyrtille(default, null): IPatch = Ref
	  .auto(hf.mode.GameMode.attachBad)
	  .wrap(function(hf, self, id, x, y, old): hf.entity.Bad {
		if (id == 25) {
		  self.badCount++;
		  return Myrtille.attach(self, x, y, false);
		}else if (id == 102) {
		  self.badCount++;
		  return BossMyrtille.attach(self, x, y);
		}else {
		  return old(self, id, x, y);
		}
  });

  @:diExport
  public var effetObjets(default, null): IPatch = Ref
  .auto(hf.SpecialManager.execute)
  .wrap(function(hf, self, item, old){
		switch (item.id)
		{
		  case Buisson.PAIX_INTERIEURE :
			for (bad in self.game.getBadClearList()) {
			  if (Std.is(bad, Myrtille)) {
				var myrtille: Myrtille = cast bad;
				if (myrtille.estInvulnerable){
				  myrtille.freezeAlways(hf.Data.FREEZE_DURATION);
				}else{
				  myrtille.freeze(hf.Data.FREEZE_DURATION * 2);
				}
			  } else {
				bad.freeze(hf.Data.FREEZE_DURATION * 2);
			  }
			}
		  case Buisson.SURPRISE_DE_PAILLE :
			//FILTRE JOUEUR
			var filtre = new etwin.flash.filters.GlowFilter();
			filtre.color = 9224447;
			filtre.alpha = 0.5;
			filtre.strength = 100;
			filtre.blurX = 2;
			filtre.blurY = 2;
			self.player.filters = [filtre];
			// EFFET FOND
			self.temporary(item.id, hf.Data.SECOND * 60);
			self.game.fxMan.attachBg(hf.Data.BG_GHOSTS, null, hf.Data.SECOND * 57);
			// EFFET FRUITS
			var listeFruits = self.game.getBadList();
			for (fruit in listeFruits) {
			  if (!Std.is(fruit, Myrtille)) {
				filtre.alpha = 1.0;
				filtre.color = 16733440;
				fruit.filters = [filtre];
			  }
			}
		  default :
			old(self,item);
		}
	});

	@:diExport
	public var finObjets(default, null): IPatch = Ref
		.auto(hf.SpecialManager.interrupt)
		.wrap(function(hf, self, id, old){
		if (id == Buisson.SURPRISE_DE_PAILLE){
		  self.actives[id] = false;
		  self.game.fxMan.clearBg();
		  var listeFruits = self.game.getBadList();
		  for (fruit in listeFruits) {
			if (!Std.is(fruit, Myrtille)) {
			  fruit.alpha = 100;
			  fruit.filters = null;
			}
		  }
		  self.player.filters = null;
		}else{
		  old(self,id);
		}
	});
	
	@:diExport
	public var introBossMyrtille(default, null): IAction = new buisson.actions.IntroBossMyrtille();
	
	@:diExport
	public var openSortie(default, null): IAction = new buisson.actions.OpenSortie();
	
	public function new(data : patchman.module.Data){
      this.fixed_background = new FixedBackground(Obfu.raw("fixed_background"));
	  var ninjaInstance = new Ninjutsu(data);
		this.ninjutsu = ninjaInstance.ninjutsuTemporaire; 
    };
}
