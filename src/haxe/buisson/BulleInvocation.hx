package buisson;

import hf.entity.Supa;
import buisson.Myrtille;
import hf.mode.GameMode;
import hf.Hf;
import etwin.Obfu;
import hf.Data;
import hf.entity.Player;
import etwin.flash.filters.GlowFilter;

@:hfTemplate
class BulleInvocation extends Supa {
	
	public static var SCALE 			: Float = 50;
	public static var TEMPS_VIE 		: Float = 7;
	public static var TEMPS_IGOR_ASSOME : Float = 1;
	public static var TEMPS_DEPART 		: Float = 1;
	
	public var player 			: hf.entity.Player;
	public var vitesse 			: Float;
	public var enervement		: Int;
	public var tempsDepart		: Float;
	
	public function new(): Void {
		super(null);
		this.fl_alphaBlink = true;
		this.blinkAlpha = 25;
		this.blinkColor = 16711680;
	}
	
	public static function attach(g: GameMode, x: Float, y: Float, e : Int, coeffVitesse : Float): BulleInvocation {
		var hf : Hf = g.root;
		hf.Std.registerClass(Obfu.raw("bulle_invocatrice"), BulleInvocation.getClass(hf));
		var bulle : BulleInvocation = cast g.depthMan.attach(Obfu.raw("bulle_invocatrice"), hf.Data.DP_SHOTS);
			bulle.player = cast g.getOne(g.root.Data.PLAYER);
			bulle.vitesse = bulle.player.speedFactor * coeffVitesse;
			bulle.setLifeTimer(BulleInvocation.TEMPS_VIE * hf.Data.SECOND);
			bulle.enervement = e;
			bulle.initSupa(g, x, y);
			bulle.scale(SCALE);
			bulle.register(hf.Data.BAD_BOMB);
			bulle.tempsDepart = BulleInvocation.TEMPS_DEPART * hf.Data.SECOND;
		return bulle;
	}
	
	override public function update(): Void {
		this.tempsDepart -= this.game.root.Timer.tmod;
		if (this.tempsDepart <= 0){
			this.moveToTarget(this.player, this.vitesse);
		}
		
		var listeJoueurs = this.game.getPlayerList();
		for (joueur in listeJoueurs){
			if(!joueur.fl_kill && !this.fl_kill && this.hitBound(joueur)){
				if (!joueur.fl_shield)
					joueur.knock(BulleInvocation.TEMPS_IGOR_ASSOME * this.game.root.Data.SECOND);
				this.fl_kill = true;
				this.destroy();
			}
		}
		super.update();
    }
	
	override public function destroy(){
		var pop = this.game.fxMan.attachFx(this.x, this.y, "popShield");
			pop.mc._xscale = SCALE;
			pop.mc._yscale = SCALE;
		this.removeMovieClip();
		super.destroy();
	}
	
	override public function onLifeTimer(): Void {
		if (this.lifeTimer <= 0){
			var myrtille : Myrtille = Myrtille.attach(this.game, this.x, this.y - 20, true);
			for (i in 0...(this.enervement)){
				myrtille.onHurryUp();
			}
			this.destroy();
		}
		super.onLifeTimer();
    }
}