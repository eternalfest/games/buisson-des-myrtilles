package buisson;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.mode.Adventure;

@:build(patchman.Build.di())
class BossLevel {
	public static var LEVEL_BOSS = 20;
	
	@:diExport
	public var bossLevel(default, null): IPatch = Ref
		.auto(hf.mode.Adventure.isBossLevel)
		.postfix(function(hf, self, id : Int, resultat : Bool){
			return id == BossLevel.LEVEL_BOSS;
		});
		
	public function new() {}
}