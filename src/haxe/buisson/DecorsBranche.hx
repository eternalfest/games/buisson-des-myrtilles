package buisson;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import utilitaire.Random;

@:build(patchman.Build.di())
class DecorsBranche {
	public static var PROBA_BRANCHE = 25;
	
	@:diExport
	public var genereBranche(default, null): IPatch = Ref
		.auto(hf.levels.View.attach)
		.after(function(hf, self){
			var level = 1541*self.world.currentId * self.world.currentId + 1;
			var seed : Random = new Random(level);
			if(self.world.scriptEngine.cycle < 10){
				for(i in 0...20){
					for(j in 0...23){
						if (self.data.__dollar__map[i][j] > 0)
						{
							// DESSUS
							if (self.data.__dollar__map[i][j-1] == 0){
								if(self.data.__dollar__map[i+1][j] == 0 && self.data.__dollar__map[i+1][j-1] == 0 && seed.random(100) < DecorsBranche.PROBA_BRANCHE)
								{
									if (seed.random(2) == 0){
										self.attachSprite("$branche01", i * 20 + 10,j* 20, true);
									}else{
										self.attachSprite("$branche06", i * 20 + 10,j* 20, true);
									}
								}
								if(self.data.__dollar__map[i-1][j] == 0 && self.data.__dollar__map[i-1][j-1] == 0 && seed.random(100) < DecorsBranche.PROBA_BRANCHE)
								{
									self.attachSprite("$branche02", i * 20 + 10,j* 20, true);
								}
							}
							// DROITE
							if (self.data.__dollar__map[i+1][j] == 0){
								if(self.data.__dollar__map[i+1][j+1] == 0 && self.data.__dollar__map[i+2][j] == 0 && seed.random(100) < DecorsBranche.PROBA_BRANCHE/2)
								{
									if (seed.random(2) == 0){
										self.attachSprite("$branche03", i * 20 + 10,j* 20, true);
									}else{
										self.attachSprite("$branche07", i * 20 + 10,j* 20, true);
									}
								}
							}
							// GAUCHE
							if (self.data.__dollar__map[i-1][j] == 0){
								if(self.data.__dollar__map[i-1][j+1] == 0 && self.data.__dollar__map[i-2][j] == 0 && seed.random(100) < DecorsBranche.PROBA_BRANCHE/2)
								{
									if (seed.random(2) == 0){
										self.attachSprite("$branche04", i * 20 + 10,j* 20, true);
									}else{
										self.attachSprite("$branche08", i * 20 + 10,j* 20, true);
									}
								}
							}
							// DESSOUS
							if (self.data.__dollar__map[i][j+1] == 0){
								if(self.data.__dollar__map[i][j+2] == 0 && self.data.__dollar__map[i][j+3] == 0 && seed.random(100) < DecorsBranche.PROBA_BRANCHE/3)
								{
									var sprite = self.attachSprite("$branche05", i * 20 + 10,j* 20, true);
								}
							}
						}
					}
				}
			}
		});
		
	public function new() {}
}