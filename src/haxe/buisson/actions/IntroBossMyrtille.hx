package buisson.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class IntroBossMyrtille implements IAction {

  public var name(default, null): String = Obfu.raw("introBossMyrtille");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
	var game = ctx.getGame();
    var boss : buisson.BossMyrtille = cast game.getList(game.root.Data.BOSS)[0];
		boss.changeIntro();
    return false;
  }
}