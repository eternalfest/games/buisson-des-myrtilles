package buisson;

import patchman.IPatch;
import merlin.IAction;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;
import utilitaire.FixedBackground;

@:build(patchman.Build.di())
class Ninjutsu {
/*
Classe de gestion du ninjutsu provisoire en attendant les zones.
N'est actuellement possible que dans le puits.
*/
  
  public var data				: patchman.module.Data;
  public var dataNinjuPuits 	: Array<Dynamic>;
  public var debut				: Int;
  public var increment			: Int;
  
  @:diExport
  public var ninjutsuTemporaire(default, null): IPatch = Ref
	  .auto(hf.mode.GameMode.onBadsReady)
	  .replace(function(hf, self): Void {
		if (self.fl_ninja && (self.getBadClearList()).length > 1) {
			var fruitACrane: hf.entity.Bad = cast self.getOne(self.root.Data.BAD_CLEAR);
				fruitACrane.fl_ninFoe = true;
			self.friendsLimit = this.debut + Math.floor(self.dimensions[0].currentId / this.increment);
			if (self.fl_nightmare) {
				++self.friendsLimit;
			}
			self.friendsLimit = Math.round(Math.min((self.getBadClearList()).length - 1, self.friendsLimit));
			while (self.friendsLimit > 0) {
				var fruitACoeur: hf.entity.Bad = cast self.getAnotherOne(self.root.Data.BAD_CLEAR, fruitACrane);
				if (!fruitACoeur.fl_ninFriend) {
					fruitACoeur.fl_ninFriend = true;
					--self.friendsLimit;
				}
			}
		}
  });
  
	public function new(data : patchman.module.Data) {
		this.data = data;
		this.dataNinjuPuits = data.get(Obfu.raw("ninjutsu"));
		for (type in this.dataNinjuPuits){
			this.increment 	= Reflect.field(type, Obfu.raw("increment"));
			this.debut 		= Reflect.field(type, Obfu.raw("debut"));
		}
	}
  
}
