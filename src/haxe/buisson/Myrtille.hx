package buisson;

import hf.entity.bad.walker.Pomme;
import hf.mode.GameMode;
import hf.Hf;
import etwin.Obfu;
import hf.Animation;
import hf.Data;
import hf.Timer;
import buisson.BulleInvocation;

import color_matrix.ColorMatrix;

@:hfTemplate
class Myrtille extends Pomme {
	
	public static var ID 				: Int = 25;
	public static var TEMPS_BOUCLIER 	: Float = 5;
	public static var TEMPS_KNOCK 		: Float = 3.5;
	public static var PROBA_DEBUT_BOUCLIER : Int = 100;
	
	public var bouclier 				: Animation;
	public var bouclierCompteur 		: Float;
	public var estInvulnerable			: Bool = false;
	public var estInvocation			: Bool;
	
	public function new(): Void {
		super(null);
		this.filters = [ColorMatrix.fromHsv(150,1, 0.8).toFilter()];
		this.animFactor = 0.65;
		this.setJumpUp(5);
		this.setJumpH(100);
		this.setClimb(100, 3);
		this.setShoot(3);
	}
	
	public static function attach(g: GameMode, x: Float, y: Float, invocation: Bool): Myrtille {
		var hf : Hf = g.root;
		hf.Std.registerClass(Obfu.raw("buisson_bad_myrtille"), Myrtille.getClass(hf));
		var bad : Myrtille = cast g.depthMan.attach(Obfu.raw("buisson_bad_myrtille"), hf.Data.DP_BADS);
			bad.initBad(g, x, y);
			bad.scale(90);
			bad.estInvocation = invocation;
			if (hf.Std.random(1000) < Myrtille.PROBA_DEBUT_BOUCLIER){
				bad.shield(null);
			}
		return bad;
	}
	
	/*
	/// GESTION BOUCLIER
	*/
	
	override public function startShoot(): Void {
		if (!this.estInvulnerable){
			super.startShoot();
		}
	}
	
	override public function dropReward(): Void {
		if(!this.estInvocation)
			super.dropReward();
	}
	
	public function shield(duration: Float): Void {
		if (duration == null) {
		  duration = Myrtille.TEMPS_BOUCLIER * this.game.root.Data.SECOND;
		}
		this.bouclier.destroy();
		this.bouclier = this.game.fxMan.attachFx(this.x, this.y, "hammer_player_shield");
		this.bouclier.fl_loop = true;
		this.bouclier.mc._xscale = 80;
		this.bouclier.mc._yscale = 80;
		this.bouclier.stopBlink();
		this.bouclierCompteur = duration;
		this.bouclier.lifeTimer = this.bouclierCompteur;
		this.estInvulnerable = true;
		
		this.slideFriction = 0.9;
		this.shockResistance = 4.0;
	}
	
	public function onShieldOut(): Void {
		if( this.bouclier != null){
			this.game.fxMan.attachFx(this.x, this.y, "popShield");
			this.bouclier.destroy();
			this.checkHits();
		}
	}
	
	public function unshield(): Void {
		this.slideFriction = null;
		this.shockResistance = 1;
		this.estInvulnerable = false;
		this.bouclierCompteur = 0;
		this.onShieldOut();
		
	}
	
	override public function endUpdate(): Void {
		if (this.bouclier != null && this.estInvulnerable) {
			this.bouclierCompteur -= this.game.root.Timer.tmod;
			if (this.bouclierCompteur <= this.game.root.Data.SECOND * 3) {
				this.bouclier.blink();
			}
			this.bouclier.mc._x += (this.x - this.bouclier.mc._x) * 0.75;
			this.bouclier.mc._y += (this.y - 15 - this.bouclier.mc._y) * 0.75;
			if (this.bouclierCompteur <= 0){
				this.unshield();
				this.bouclier = null;
			}
			if (!this.fl_hitGround)
				this.fl_hitGround = true;
		}
		super.endUpdate();
	}
	override public function onShoot(): Void {
		this.shield(Myrtille.TEMPS_BOUCLIER * this.game.root.Data.SECOND);
    }
	
	/*
	/// GESTION INVULNERABILITE
	*/
	
	override public function freeze(duration: Float): Void {
		if (!this.estInvulnerable ){
			super.freeze(duration);
		}else{
			super.knock(Myrtille.TEMPS_KNOCK * this.game.root.Data.SECOND);
		}
    }
	
	public function freezeAlways(duration: Float): Void {
		if (this.estInvulnerable){
			this.unshield();
		}
		super.freeze(duration);
    }
	
	override public function killHit(dx: Null<Float>): Void {
		if (!this.estInvulnerable){
			super.killHit(dx);
		}else{
			super.knock(Myrtille.TEMPS_KNOCK * this.game.root.Data.SECOND);
		}
    }
	override public function forceKill(dx: Null<Float>): Void {
		if (this.estInvulnerable){
			this.unshield();
			super.forceKill(0);
		}else{
			super.forceKill(0);
		}
    }
	override public function destroy(): Void {
		if (this.estInvulnerable){
			this.unshield();
		}
		super.destroy();
    }

	override public function burn(): Void {
		if (this.estInvulnerable){
			this.unshield();
		}
		super.burn();
    }
}